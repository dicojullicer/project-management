import { Component, ViewChild } from '@angular/core';
import { Platform, Nav } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Events } from 'ionic-angular';
import { Auth } from '../providers/auth';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {

  @ViewChild(Nav) nav: Nav;

  constructor(private platform: Platform, 
              private statusBar: StatusBar, 
              private splashScreen: SplashScreen,
              public events: Events,
              private auth: Auth) {

    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();

      this.events.publish('app:init');
      this.events.subscribe('user:logged_in', (user) => this.checkUser(user));
      this.events.subscribe('user:logged_out', (user) => this.checkUser(user)); 
      this.events.subscribe('user:guest', (user) => this.checkUser(user));  
    });
  }

  checkUser(user)
  {
    this.splashScreen.hide();
  
    if (user) this.nav.setRoot('projects');
    else this.nav.setRoot('login');
  }
}

