import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Alert, AlertController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Events } from 'ionic-angular';
import { Project } from '../../providers/project';
/**
 * Generated class for the CreateProject page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage({
	name: 'create-project',
	segment: 'projects/create'
})

@Component({
  selector: 'page-create-project',
  templateUrl: 'create-project.html',
})

export class CreateProjectPage {

	form: FormGroup;

	isSaving: boolean = false;

  	constructor(public navCtrl: NavController, 
  				public navParams: NavParams,
  				public formBuilder: FormBuilder,
  				private repo: Project,
  				public events: Events,
          public alertCtrl: AlertController) 
  	{
      
  		this.form = this.formBuilder.group({
  			name: ['', Validators.compose([Validators.required, Validators.minLength(3)])]
  		});
      this.events.subscribe('project:creating', () => this.isSaving = true);
      this.events.subscribe('project:created', () => this.isSaving = false);
  	}

  	ionViewDidLoad() {
    	console.log('ionViewDidLoad CreateProject');
  	}

  	save()
  	{	
      let page = this, 
        project = this.form.value;

      this.repo.create(project)
        .then((saved) => {
          page.navCtrl.setRoot('projects', {new: saved});
        })
        .catch((reason) => this.showAlert(reason));
          
      return false;
  	}

    showAlert(message)
    {
      let msg = typeof message === 'object' ? JSON.stringify(message) : message,
        alert: Alert = this.alertCtrl.create({
          title: "Projects",
          message: msg,
          buttons: ['dismiss']
        });

      alert.present();
    }
}