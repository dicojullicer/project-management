import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Alert, AlertController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { Events } from 'ionic-angular';
import { Auth } from '../../providers/auth';

export class EmailValidator {
 
  static isValid(field: FormControl){
  	
  	return field.value.match(/^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i)
  		? null : { valid_email: false };
  
  } 
}

/**
 * Generated class for the Login page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage({
	name: 'login',
	segment: 'login'
})
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  form: FormGroup;    

  loginFailedAttempts: number = 0;

  isLoggingIn: boolean = false;

  constructor(public navCtrl: NavController, 
  			  public navParams: NavParams,
  			  public formBuilder: FormBuilder,
  			  private auth: Auth,
  			  public events: Events,
          public alertCtrl: AlertController) {

  	this.form = this.formBuilder.group({
  		mail: ['admin@scale.com', Validators.compose([Validators.required, Validators.minLength(4), EmailValidator.isValid])],
  		password: ['elacs', Validators.compose([Validators.required, Validators.minLength(4)])],
  	});

  	this.events.subscribe('user:logged_in', (user) => this.loginFailedAttempts = 0);
  	this.events.subscribe('user:logging_in', (mail, pass) => this.isLoggingIn = true);
  }

  login() 
  {
  	let credentials = this.form.value;

  	this.auth.attempt(credentials.mail, credentials.password)
      .catch((reason) => this.loginFailed(reason))

  	// don't submit form maybe only on browser platform ?
  	// otherwise just making sure we don't submit the form or we'll
  	// messed up
  	return false;
  }

  loginFailed(reason)
  {
  	this.isLoggingIn = false;
  	this.loginFailedAttempts++;
    this.showAlert(reason);
  }

  showAlert(message)
    {
      let msg = typeof message === 'object' ? JSON.stringify(message) : message,
        alert: Alert = this.alertCtrl.create({
          title: "Login",
          message: msg,
          buttons: ['dismiss']
        });

      alert.present();
    }
}
