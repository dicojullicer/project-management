import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Alert, AlertController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Events } from 'ionic-angular';
import { Project } from '../../providers/project';

/**
 * Generated class for the EditProject page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage({
	name: 'edit-project',
	segment: 'projects/:id/edit'
})
@Component({
  selector: 'page-edit-project',
  templateUrl: 'edit-project.html',
})
export class EditProject {

	project: any;

	form: FormGroup;

	isSaving: boolean = false;

  	constructor(public navCtrl: NavController, 
  				public navParams: NavParams,
  				private repo: Project,
  				public events: Events,
          		public alertCtrl: AlertController,
          		public formBuilder: FormBuilder) {

  		let page = this,
  			id = this.navParams.get('id');

  		this.form = this.formBuilder.group({
  			name: ['', Validators.compose([Validators.required, Validators.minLength(3)])]
  		});

  		this.repo.findBy('id', id)
  			.then((project) => {
  				page.project = project;
  				page.form.setValue({name: page.project.name});
  			})
  			.catch((reason) => {
  				page.showAlert("Failed to load Project");
  				page.navCtrl.setRoot('projects');
  			});

  		
  	}

  	ionViewDidLoad() {
    	console.log('ionViewDidLoad EditProject');
  	}

  	save()
  	{	
  	  let page = this;
      this.repo.update(this.project.id, this.form.value)
      	.then((updated) => {
      		page.navCtrl.setRoot('projects', {updated: updated});
      	})
      	.catch((reason) => page.showAlert("Failed to updated project"));
      
      return false;
  	}

    showAlert(message)
    {
      let msg = typeof message === 'object' ? JSON.stringify(message) : message,
        alert: Alert = this.alertCtrl.create({
          title: "Projects",
          message: msg,
          buttons: ['dismiss']
        });

      alert.present();
    }

}
