import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Alert, AlertController } from 'ionic-angular';
import { ItemSliding } from 'ionic-angular';
import { Auth } from '../../providers/auth';
import { Project } from '../../providers/project';
/**
 * Generated class for the Projects page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage({
  name: 'projects',
  segment: 'projects'
})
@Component({
  selector: 'page-projects',
  templateUrl: 'projects.html',
})
export class ProjectPage {

  projects: Object[] = [];

  constructor(public navCtrl: NavController, 
    public navParams: NavParams, 
    private auth: Auth, 
    private repo: Project,
    private alertCtrl: AlertController) {
    
    this.getProjects();
  }

  getProjects()
  {
    this.repo.getAll()
      .then((projects) => this.projects = projects)
      .catch((reason) => console.log(reason));
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Projects');
  }

  logout()
  {
  	this.auth.logout();
  }

  createProject()
  {
    this.navCtrl.push('create-project');
  }

  editProject(project)
  {
    this.navCtrl.push('edit-project', {id: project.id});
  }

  deleteProject(project, slideItem: ItemSliding)
  {
    this.repo.remove(project.id)
      .then((id) => this.getProjects())
      .catch((reason) => this.showAlert(reason));
    
  }

  showAlert(message)
    {
      let msg = typeof message === 'object' ? JSON.stringify(message) : message,
        alert: Alert = this.alertCtrl.create({
          title: "Projects",
          message: msg,
          buttons: ['dismiss']
        });

      alert.present();
    }
}
