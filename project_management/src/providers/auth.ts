import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

import { Events } from 'ionic-angular';
import { Storage } from '@ionic/storage';

export let mock_user = {
	mail: 'admin@scale.com',
	password: 'elacs'
}


@Injectable()
export class Auth {

    private user: any;

    constructor(public http: Http, private storage: Storage, public events: Events) 
    {
    	this.events.subscribe('app:init', () => this.initGetUserFromStorage());
    }

    findByCredentials(mail, password)
    {
        return mock_user.mail == mail
            && mock_user.password == password;  
    }

    attempt(mail: string, password: string): Promise<Object>
    {
        let auth = this;
        
        // event trigger user:logging_in
        auth.events.publish('user:logging_in', [mail, password]);

        return new Promise((resolve, reject) => {
            if (auth.findByCredentials(mail, password))
            {
                setTimeout(() => {
                    auth.storage.set('user', mock_user)
                        .then(() => {
                            auth.user = mock_user;
                            auth.events.publish('user:logged_in', mock_user);
                            resolve(mock_user);
                        })
                        .catch((reason) => {
                            auth.events.publish('user:attempt:failed', reason);
                            reject(reason);
                        });
                    }, 200)
                
            }

            else {
                auth.events.publish('user:attempt:failed', "Invalid mail/password");
                reject("Invalid mail/password");
            }
        });
    }

    check()
    {
        return !!this.user;
    }

	User()
	{
        return this.user;
  	}

    logout()
    {    
        // event trigger user:logging_out
        // @todo if halted then cancel logout
        this.storage.remove('user')
            .then(() => {
                this.user = null;
                this.events.publish('user:logged_out')
            });
    }  

    /**
    * 
    */  
    initGetUserFromStorage() 
    {
        this.storage.get('user')
            .then((user) => {
                this.user = user;
                this.events.publish('user:logged_in', user);
            })
            .catch(() => {
                this.events.publish('user:guest');
            });
    }

}
