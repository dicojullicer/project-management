import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { Events } from 'ionic-angular';
import { Storage } from '@ionic/storage';


/*
  Generated class for the Projects provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class Project {
	projects: Object[] = [];
  	
  	constructor(public http: Http, public events: Events, private storage: Storage) {}

  	getAll() 
  	{
  		/**
  		* Should we make this event driven provider ?
  		* remember this is like a repo for projects so 
  		* we rather return promise 
  		*/
  		return this.storage.get('projects')
  			.then((projects) => this.projects = projects || [])
  			.catch((reason) => this.projects = []);
  	}

    findBy(field, value): Promise<Object>
    {
      let repo = this;

      return new Promise((resolve, reject) => {
        this.getAll()
          .then((projects) => {
            let projs = projects || [],
                project: any = null;

             for(var i in projs)
             {
               let base = projs[i][field];

               if (base && base.toString().toLowerCase() == value.toString().toLowerCase())
               {
                  project = projs[i];
                  break;
               }
             }
             
             if (project) resolve(project);
             else resolve(project);
          })
          .catch((reason) => () => {
            reject(reason);
          });
      });
    }

  	load(id): Promise<Object>
  	{
      return this.findBy('id', id);
  	}

  	create(data): Promise<Object>
  	{
  		let repo = this,
  			project: any = <Object>data;

  		this.events.publish('project:creating', project);

  		return new Promise((resolve, reject) => {
        repo.findBy('name', project.name)
          .then((found) => {
            if (found)
            {
              this.events.publish('project:create:failed', project);
              reject("Project already exists!");
            }
            else
            {
              repo.saveProject(project)
                .then((project) => resolve(project))
                .catch((reason) => {
                  this.events.publish('project:create:failed', project);
                  reject(reason);
                });
            }
          })
          .catch((reason) => {
            this.events.publish('project:create:failed', project);
            reject(reason);
          })
  		});
  	}

  	update(id, data)
  	{
      let repo = this;

      repo.events.publish('project:updating', data);

      return new Promise((resolve, reject) => {

        repo.findBy('id', id)
          .then((project) => {
            if (!project) {
              repo.events.publish('project:update:failed', data);
              return reject("Project not found");
            }
            let proj: any = project,
            updated = Object.assign(project, data);

            repo.findBy('name', proj.name)
              .then((found) => {
                let exists: any = found;

                if (exists && exists.id != proj.id)
                {
                  repo.events.publish('project:update:failed', data);
                  return reject("Project name duplicates");
                }

                repo.saveProject(updated)
                  .then((proj) => resolve(proj))
                  .catch((reason) => reject(reason));
              });
          })
          .catch((reason) => {
            repo.events.publish('project:update:failed', data);
            reject(reason);
          });
      });
  	}

  	remove(id)
  	{
      let repo = this;
      return new Promise((resolve, reject) => {
        repo.findBy('id', id)
          .then((project) => {
            let index: any = repo.findIndex(id),
              old: any;

            if (project && index !== false) {
              old = repo.projects[index];
              repo.projects.splice(index, 1);
              repo.storage.set('projects', repo.projects)
                .then((updated) => {
                  repo.events.publish('project:deleted', id);
                  resolve(id);
                })
                .catch((reason) => {

                  if (index !== false && old)
                    repo.projects[index] = old;

                  repo.events.publish('project:delete:failed', id);
                  reject(reason);
                });
            }
            else {
              repo.events.publish('project:delete:failed', id);
              return reject("Project not found");
            }
          })
          .catch((reason) => {
            repo.events.publish('project:update:failed', id);
            reject(reason);
          });
      });
  	}

    private findIndex(id)
    {
      for(var i in this.projects)
      {
        let proj: any = this.projects[i];

        if (proj.id == id)
        {
          return i;
        }
      }

      return false;
    }

    private saveProject(project: any): Promise<Object>
    {
      let repo = this, 
        create = !project.id,
        event = create ? 'created' : 'updated',
        index: any,
        old: any;
      
      if (create) {
        project.id = Date.now();
        repo.projects.push(project);
      }
      else {
        index = this.findIndex(project.id);

        if (index === false)
        {
          return new Promise((resolve, reject) => reject("Project not found"));
        }
        old = repo.projects[index];
        repo.projects[index] = project;
      }
      
      return repo.storage.set('projects', repo.projects)
        .then(() => {
          
          repo.events.publish(event, project);
        })
        .catch((reason) => () => {
          if (create)
            repo.projects.pop();
          else if (!create && index !== false && old)
            repo.projects[index] = old;

          repo.events.publish('project:save:failed', project);
        });
    }

}
